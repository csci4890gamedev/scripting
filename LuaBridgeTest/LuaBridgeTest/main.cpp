//
//  main.cpp
//  LuaTest
//
//  Created by Eugene Sturm on 1/31/13.
//  Copyright (c) 2013 Eugene Sturm. All rights reserved.
//


// Include the lua headers (the extern "C" is a requirement because we're
// using C++ and lua has been compiled as C code)

extern "C" {
#include "lua.h"
#include "lualib.h"
#include "lauxlib.h"
}




#include <iostream>
#include <string>
#include "Luabridge.h"


extern "C" {
    static int l_cppfunction(lua_State *L) {
        double arg = luaL_checknumber(L,1);
        lua_pushnumber(L, arg * 0.5);
        return 1;
    }
}

int globalVar;
static float staticVar;

std::string stringProperty;
std::string getString () { return stringProperty; }
void setString (std::string s) { stringProperty = s; }

int foo () { return 42; }
void bar (char const*) { }
int cFunc (lua_State* L) { return 0; }
using namespace std;

int main()
{
   
    cout << "** Test Lua embedding" << endl;
    cout << "** Init Lua" << endl;
    lua_State *L;
  
    L = luaL_newstate();
    luaL_openlibs(L);
    
    luabridge::getGlobalNamespace(L)
    .beginNamespace("test")
        .addVariable("var1", &globalVar)
        .addVariable("var2", &staticVar, false)
        .addFunction("foo", foo)
        .addFunction("bar", bar)
        .addCFunction("cfunc", cFunc)
    .endNamespace();
    
    cout << "** Load chunk. without executing it" << endl;
    if (luaL_loadfile(L, "/Users/sturm/Projects/GameEngine/scripts/luascript2.lua")) {
        cerr << "Something went wrong loading the chunk (syntax error?)" << endl;
        cerr << lua_tostring(L, -1) << endl;
        lua_pop(L,1);
    }
    
    /*cout << "** Make a insert a global var into Lua from C++" << endl;
    lua_pushnumber(L, 1.1);
    lua_setglobal(L, "cppvar");*/
    
    cout << "** Execute the Lua chunk" << endl;
    if (lua_pcall(L,0, LUA_MULTRET, 0)) {
        cerr << "Something went wrong during execution" << endl;
        cerr << lua_tostring(L, -1) << endl;
        lua_pop(L,1);
    }
    /*
    cout << "** Read a global var from Lua into C++" << endl;
    lua_getglobal(L, "luavar");
    double luavar = lua_tonumber(L,-1);
    lua_pop(L,1);
    cout << "C++ can read the value set from Lua luavar = " << luavar << endl;
    
    cout << "** Execute a Lua function from C++" << endl;
    lua_getglobal(L, "myluafunction");
    lua_pushnumber(L, 5);
    lua_pcall(L, 1, 1, 0);
    cout << "The return value of the function was " << lua_tostring(L, -1) << endl;
    lua_pop(L,1);
    
    cout << "** Execute a C++ function from Lua" << endl;
    cout << "**** First register the function in Lua" << endl;

    lua_pushcfunction(L,l_cppfunction);
    lua_setglobal(L, "cppfunction");
    
    cout << "**** Call a Lua function that uses the C++ function" << endl;
    lua_getglobal(L, "myfunction");
    lua_pushnumber(L, 5);
    lua_pcall(L, 1, 1, 0);
    cout << "The return value of the function was " << lua_tonumber(L, -1) << endl;
    lua_pop(L,1);
    
    cout << "** Release the Lua enviroment" << endl;*/
    lua_close(L);
}
